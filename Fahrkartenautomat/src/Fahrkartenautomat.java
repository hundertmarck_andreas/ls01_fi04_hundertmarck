﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double rueckBetrag;
       
       while(true) {

       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       

       // Geldeinwurf
       // -----------
       
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);


       // Fahrscheinausgabe
       // -----------------
       
       fahrkartenAusgeben();


       // Rückgeldberechnung und -Ausgabe
       // -------------------------------

       rueckBetrag = rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");	}}
    
    public static void fahrkartenmenue(String[] fahrkartenname, double[] fahrkartenpreise) {
//    	double zuZahlenderBetrag;
	    System.out.println("Fahrkartenbestellvorgang:");
	    System.out.println("=========================");
	    System.out.println("");
	    System.out.println("Wählen Sie Ihre Fahrkarte aus: ");
	    
	    for(int i = 0; i < fahrkartenname.length; i++) {
	    	System.out.println(fahrkartenname[i] + " - " + fahrkartenpreise[i] + "€");
	    }
	    
	    System.out.println("");
	    System.out.println("Ihre Wahl: ");
//	    int nutzerWahl = tastatur.nextInt();
//	    switch(nutzerWahl) {
//	    case 1: zuZahlenderBetrag = 2.9;
//	    	break;
//	    case 2: zuZahlenderBetrag = 7.6;
//	    	break;
//	    case 3: zuZahlenderBetrag = 9.5;
//	    	break;
//	    default: System.out.println("Fehler bei der Eingabe!");
//	    	zuZahlenderBetrag = 2.9;
//	    }
//	     
//	    return zuZahlenderBetrag;
    } 

    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	String[] fahrkartenname = new String[] {"1. Einzelfahrschein Berlin AB", "2. Einzelfahrschein Berlin BC", "3. Einzelfahrschein Berlin ABC", "4. Kurzstrecke", "5. Tageskarte Berlin AB", "6. Tageskarte Berlin BC", "7. Tageskarte Berlin ABC", "8. Kleingruppen-Tageskarte Berlin AB", "9. Kleingruppen-Tageskarte Berlin BC", "10. Kleingruppen-Tageskarte Berlin ABC"};
    	double[] fahrkartenpreise = new double[] {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	fahrkartenmenue(fahrkartenname, fahrkartenpreise);
    	
    	double zuZahlenderBetrag = fahrkartenpreise[(tastatur.nextInt() - 1)];

    	System.out.print("Anzahl der Tickets: ");
    	int[] ticketCounter = new int[] {tastatur.nextInt()};

    	if (ticketCounter[0] < 1 || ticketCounter[0] > 10) {
    		ticketCounter[0] = 1;

    		System.out.println("Fehler: Dieser Automat nimmt nur eine Anzahl an Tickets zwischen 1 und 10 an.");
    	}
    	return zuZahlenderBetrag *= ticketCounter[0];

    }

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {

			System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
				warte(250);
	       }
	       System.out.println("\n\n");
		}
	
	public static void warte(int millisekunden) {
	          try {
				Thread.sleep(millisekunden);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	
    public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	double rueckBetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	
    	if (rueckBetrag > 0.0){
    	    System.out.println("Der Rückgabebetrag in Höhe von " + rueckBetrag + " EURO");
    	    System.out.println("wird in folgenden Münzen ausgezahlt:");

    	      while(rueckBetrag >= 2.0) // 2 EURO-Münzen
    	      {
    	      System.out.println("2 EURO");
    		          rueckBetrag -= 2.0;
    	           }
    	           while(rueckBetrag >= 1.0) // 1 EURO-Münzen
    	           {
    	        	  System.out.println("1 EURO");
    		          rueckBetrag -= 1.0;
    	           }
    	           while(rueckBetrag >= 0.5) // 50 CENT-Münzen
    	           {
    	        	  System.out.println("50 CENT");
    		          rueckBetrag -= 0.5;
    	           }
    	           while(rueckBetrag >= 0.2) // 20 CENT-Münzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          rueckBetrag -= 0.2;
    	           }
    	           while(rueckBetrag >= 0.1) // 10 CENT-Münzen
    	           {
    	        	  System.out.println("10 CENT");
    		          rueckBetrag -= 0.1;
    	           }
    	           while(rueckBetrag >= 0.05)// 5 CENT-Münzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          rueckBetrag -= 0.05;
    	           }
    	       }

    	return rueckBetrag;
    }  }

	 