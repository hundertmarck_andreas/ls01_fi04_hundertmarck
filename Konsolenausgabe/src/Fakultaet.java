
public class Fakultaet {

	public static void main(String[] args) {
		
		int i = 0;
		
		System.out.printf("%-5s", i + "!").printf("%-20s", "=").printf("%s", "=").printf("%5s", " 1" + "\n");
		i++;
		System.out.printf("%-5s", i + "!").printf("%-20s", "= 1").printf("%s", "=").printf("%5s", " 1" + "\n");
		i++;
		System.out.printf("%-5s", i + "!").printf("%-20s", "= 1 * 2").printf("%s", "=").printf("%5s", " 2" + "\n");
		i++;
		System.out.printf("%-5s", i + "!").printf("%-20s", "= 1 * 2 * 3").printf("%s", "=").printf("%5s", " 6" + "\n");
		i++;
		System.out.printf("%-5s", i + "!").printf("%-20s", "= 1 * 2 * 3 * 4").printf("%s", "=").printf("%5s", " 24" + "\n");
		i++;
		System.out.printf("%-5s", i + "!").printf("%-20s", "= 1 * 2 * 3 * 4 * 5").printf("%s", "=").printf("%5s", "120" + "\n");
		i++;
	}
}
