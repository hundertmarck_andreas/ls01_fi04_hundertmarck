import java.util.Scanner;

public class sum {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		System.out.println("A: Geben Sie eine Zahl ein (Summe in n-Schritten): ");
		int n = eingabe.nextInt();
		int sum = 0;
		
		for(int i = 1; i <= n; i++) {
			sum = (n *(i+1))/2 ;
		}
		System.out.println("Die Summe f�r A betr�gt: " + sum);
		
		
		System.out.println("B: Geben Sie eine Zahl ein (Summe in 2n-Schritten): ");
		int l = eingabe.nextInt();
		
		for(int i = 2; i <= l; i++) {
			sum = (l * (i+2))/4 ;
		}
		System.out.println("Die Summe f�r B betr�gt: " + sum);
		
		System.out.println("C: Geben Sie eine Zahl ein (Summe in 2n+1-Schritten): ");
		int j = eingabe.nextInt();
		
		for(int i = 1; i <= j; i++) {
			sum = (j * (i+2))/5 ;
		}
		System.out.println("Die Summe f�r C betr�gt: " + sum);
	}

}