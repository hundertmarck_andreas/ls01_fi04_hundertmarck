
public class Folgen {

	public static void main(String[] args) {

		System.out.println("Aufgabe A:");
		for(int i = 99; i >= 9; i-= 3) {
			System.out.print(i + ", ");
		}
		System.out.println("\nAufgabe B:");
		for (int j = 1; j <= 20; j++) {
			System.out.print((j * j) + ", ");
		}
		System.out.println("\nAufgabe C:");
		for (int k = 2; k <= 102; k+=4) {
			System.out.print(k + ", ");
		}
		System.out.println("\nAufgabe D:");
		for(int l=2; l<=32; l++){
			if(l % 2 == 0){
				int calc = l * l;
				System.out.print(calc + ", ");
			}
		}
		System.out.println("\nAufgabe E:");
		for (int m = 1; m <= 32768; m++) {
			if(m % 2 == 0) {
				int mult = m * 2;
				int fact = mult * 2;
				System.out.print(fact + ", ");
		}
	}
}}
